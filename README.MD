[![pipeline status](https://gitlab.com/jonasdaviladasilva/lojinha-cypress/badges/master/pipeline.svg)](https://gitlab.com/jonasdaviladasilva/lojinha-cypress/-/commits/master)

# Este projeto tem a finalidade de exercitar o padrão app action.

## Clone o projeto e rode os segintes comandos

Já na pasta do projeto rode o comando

    npm install

Comando para rodar pelo electron browser :

    npm run cypress:open

Comando para rodar em headless mode:

     npm run cypress:run
